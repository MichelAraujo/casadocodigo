var express = require('../config/express')();
var supertest = require('supertest')(express);

describe('#ProdutosController', function () {
    it('#listagem json', function (done) {

        supertest.get('/produtos')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });

    it('#listagem HTML', function (done) {
        supertest.get('/produtos')
            .set('Accept', 'text/html')
            .expect('Content-Type', /html/)
            .expect(200, done);
    });

    it('#cadastro de novo produto com dados invalidos', function (done) {
        supertest.post('/produtos')
            .send({titulo:"", descricao:"novo livro"})
            .expect(400, done);
    });

    it('#cadastro de novo', function (done) {
        supertest.post('/produtos')
            .send({titulo:"Titulo novo", descricao:"novo livro", preco:50})
            .expect(302, done);
    });
});