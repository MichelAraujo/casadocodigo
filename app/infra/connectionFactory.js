var mysql = require('mysql');

function createDBConnection() {
    return mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'root', //TIl0xg8Jmb,O
        database : 'casadocodigo_nodejs'
    });
}

module.exports = function () {
    return createDBConnection;
};