var express = require('./config/express')();
var http = require('http').Server(express);
var io = require('socket.io')(http);

express.set('io', io);

http.listen(3000, function() {
	console.log("Servidor rodando");
});